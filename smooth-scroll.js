/**
 * Smoothly scrolls to elements it's called upon
 * Returns Jquery object for chaining
 */
$.fn.smoothScrollTo = function(speed, offset) {
	if (typeof speed === 'undefined') speed = 800;
	if (typeof offset === 'undefined') offset = 100;
	$('html, body').animate({
		scrollTop: this.offset().top - offset // Adjust to change final scroll position top margin
	}, speed); // Adjust to change animations speed (ms)
	return this;
};